package com.icecarev.seminarwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeminarWorkApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeminarWorkApplication.class, args);
	}

}
