package com.icecarev.seminarwork.controller;

import com.icecarev.seminarwork.config.JwtTokenUtil;
import com.icecarev.seminarwork.model.ApiResponse;
import com.icecarev.seminarwork.model.AuthToken;
import com.icecarev.seminarwork.model.Employee;
import com.icecarev.seminarwork.model.LoginUser;
import com.icecarev.seminarwork.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/token")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @PostMapping(value = "/generate-token")
    public ApiResponse<AuthToken> register(@RequestBody LoginUser loginUser) {

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUser.getUsername(), loginUser.getPassword()));
        final Employee employee = userService.findOne(loginUser.getUsername());
        final String token = jwtTokenUtil.generateToken(employee);
        return new ApiResponse<>(200, "success",new AuthToken(token, employee.getUsername()));
    }

}
