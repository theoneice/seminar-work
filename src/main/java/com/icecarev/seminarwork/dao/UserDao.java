package com.icecarev.seminarwork.dao;

import com.icecarev.seminarwork.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<Employee, Integer> {

    Employee findByUsername(String username);
}
