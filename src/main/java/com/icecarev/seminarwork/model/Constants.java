package com.icecarev.seminarwork.model;

public final class Constants {

    private Constants() {}

    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5L * 60 * 60;
    public static final String SIGNING_KEY = "icecarev1234";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
