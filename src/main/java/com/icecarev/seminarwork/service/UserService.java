package com.icecarev.seminarwork.service;

import com.icecarev.seminarwork.model.Employee;
import com.icecarev.seminarwork.model.UserDto;

import java.util.List;

public interface UserService {

    Employee save(UserDto user);
    List<Employee> findAll();
    void delete(int id);

    Employee findOne(String username);

    Employee findById(int id);

    UserDto update(UserDto userDto);
}
