package com.icecarev.seminarwork.service.impl;

import com.icecarev.seminarwork.dao.UserDao;
import com.icecarev.seminarwork.model.Employee;
import com.icecarev.seminarwork.model.UserDto;
import com.icecarev.seminarwork.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {
	
	@Autowired
	private UserDao userDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Employee employee = userDao.findByUsername(username);
		if(employee == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(employee.getUsername(), employee.getPassword(), getAuthority());
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public List<Employee> findAll() {
		List<Employee> list = new ArrayList<>();
		userDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(int id) {
		userDao.deleteById(id);
	}

	@Override
	public Employee findOne(String username) {
		return userDao.findByUsername(username);
	}

	@Override
	public Employee findById(int id) {
		Optional<Employee> optionalUser = userDao.findById(id);
		return optionalUser.isPresent() ? optionalUser.get() : null;
	}

    @Override
    public UserDto update(UserDto userDto) {
        Employee employee = findById(userDto.getId());
        if(employee != null) {
            BeanUtils.copyProperties(userDto, employee, "password");
            userDao.save(employee);
        }
        return userDto;
    }

    @Override
    public Employee save(UserDto user) {
	    Employee newEmployee = new Employee();
	    newEmployee.setUsername(user.getUsername());
	    newEmployee.setFirstName(user.getFirstName());
	    newEmployee.setLastName(user.getLastName());
	    newEmployee.setPassword(bcryptEncoder.encode(user.getPassword()));
		newEmployee.setAge(user.getAge());
		newEmployee.setSalary(user.getSalary());
        return userDao.save(newEmployee);
    }
}
