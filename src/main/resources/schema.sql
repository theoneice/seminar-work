drop table employee;
CREATE TABLE IF NOT EXISTS employee (
    id serial not null,
    first_name character varying(255) not null,
    last_name character varying(255) not null,
    username character varying(255) not null,
    password character varying(255) not null,
    salary integer not null,
    age integer not null,
    constraint user_pkey primary key (id)
);